#Loom - Adeliom Animation Core

Work with animation class and also compatible with [Animate.css](https://daneden.github.io/animate.css/)

## How to build Loom

```
$ git clone https://bitbucket.org/adeliomgit/loom && cd loom
$ npm install
$ grunt
```

## How to install Loom

###With npm
```
$ npm install https://bitbucket.org/adeliomgit/loom.git --save
```

###With bower
```
$ bower install https://bitbucket.org/adeliomgit/loom.git --save
```

## How to use Loom

Loom work with attributes

| Attributes    | Usage       |
| ------------- |-------------|
| loom     | loom="fadeInUp" |
| loom-delay      | loom-delay="400ms" (0 by default)    |
| loom-duration | loom-duration="600ms" (800ms by default)   |
