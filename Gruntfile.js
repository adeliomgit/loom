module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'loom.min.js': ['loom.js']
        }
      }
    },
    jshint: {
      files: ['Gruntfile.js', 'loom.js'],
      options: {
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true,
          browser: true,
        }
      }
    },
    watch: {
      files: ['loom.js'],
      tasks: ['jshint', 'uglify']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['jshint', 'uglify']);

};