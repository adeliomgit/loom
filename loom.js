/**
 *
 * toggle class when element is in viewport, use to animate in css when element is visible
 *
 *********************/

var Loom = (function() {
    'use strict';
    var triggerElementSelector = '[loom]';
    var visibleClass = 'animated';

    var elements = [];

    var fps = 60;
    var now;
    var then = Date.now();
    var interval = 1000 / fps;
    var delta;

    var isFirst = true;

    var isScrolling = true;


    var App = {
        scrollTop: 0,
        setScrollTop: function() {
            this.scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        },

        previousScroll: 0,
        setPreviousScroll: function() {
            this.previousScroll = this.scrollTop;
        },

        isScrolling: function() {
            return this.previousScroll != this.scrollTop;
        },
        windowHeight: window.innerHeight,
        documentHeight: document.documentElement.offsetHeight
    };
    App.setScrollTop();

    var isInViewport = function(element, rec) {
        var modifier = -200;
        var rect = rec || element.getBoundingClientRect();
        return rect.bottom > 0 && (rect.top - modifier) < App.windowHeight;
    };

    //compatible requestAnimationFrame function
    // http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
    var requestAnimFrame = function(callback) {
        return window.requestAnimationFrame(callback) ||
            window.webkitRequestAnimationFrame(callback) ||
            window.mozRequestAnimationFrame(callback) ||
            window.oRequestAnimationFrame(callback) ||
            window.msRequestAnimationFrame(callback) ||
            function(callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    };


    var loop = function() {

        requestAnimFrame(loop);

        now = Date.now();
        delta = now - then;

        if (delta > interval && isScrolling) {

            then = now - (delta % interval);

            if (isScrolling) {
                App.setScrollTop();
            }

            //scroll event
            if (App.previousScroll != App.scrollTop || isFirst) {

                triggerVisible();

                if (isFirst) {
                    isFirst = false;
                }
            }

            isScrolling = false;


        }
    };

    var doAnimations = function(elements) {
        var $this = elements;
        var $animationDelay = ($this.getAttribute('loom-delay')) ? $this.getAttribute('loom-delay') : "0";
        var $animationDuration = ($this.getAttribute('loom-duration')) ? $this.getAttribute('loom-duration') : "800ms";
        var $animationType = $this.getAttribute('loom');

        $this.style.animationDelay = $animationDelay;
        $this.style.animationDuration = $animationDuration;
        $this.style.visibility = 'visible';

        $this.classList.add($animationType);
        $this.classList.add(visibleClass);
    };

    var triggerVisible = function() {

        if (elements.length) {
            var remainingElements = [];

            for (var i = 0; i < elements.length; i++) {
                var el = elements[i];
                var rect = el.getBoundingClientRect();
                var inViewport = isInViewport(el, rect);

                var keep = false;

                if (inViewport && !el.classList.contains(visibleClass)) {
                    doAnimations(el);
                } else {
                    keep = true;
                }

                if (keep) {
                    remainingElements.push(el);
                }
            }

            elements = remainingElements;
        }
    };


    var init = function() {
        var tmpElements = document.querySelectorAll(triggerElementSelector);

        for (var i = 0, nbElements = tmpElements.length; i < nbElements; i++) {
            tmpElements[i].style.visibility = 'hidden';
            elements.push(tmpElements[i]);
        }

        window.addEventListener('scroll', function() {
            isScrolling = true;
        });

        loop();
    };

    return {
        init: init,
        triggerVisible: triggerVisible
    };

})();